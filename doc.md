# Documentation généralisation de Stylo (29-30 janvier 2019)

## sur le schéma yaml : 
- vérifier array of people
- on décide de rendre tout éditable dans le yaml editor, avec la possibilité de mettre un préremplissage à partir des données du yaml dans le profil.
- multilangue pour les mot-clés (frontend yamleditor)
- controlledKeywords : à améliorer en le branchant sur les api isidore ou sphub
- vérifier la gestion langue it/ita dans les templates latex
- [x] chgt id_sp > id
- [x] chgt url_article_sp > url_article
- [x] redacteurDossier > issueDirectors
- translator > translators
- ajout transcriber 
- orig_lang&translation > translationOf : plus de langue originale, seuls les textes qui sont des traductions pointent vers le texte original
- suppr articleslies

- pour la XSLT, si pas de dossier, ajouter la balise varia



## TRANSFO XSLT ERUDIT.

script bash saxon-xslt
```bash
#!/bin/sh
exec java -cp /usr/share/maven-repo/net/sf/saxon/Saxon-HE/9.8.0.8/Saxon-HE-9.8.0.8.jar:/usr/bin/tagsoup-1.2.1.jar net.sf.saxon.Transform -x:org.ccil.cowan.tagsoup.Parser  "$@" \!indent=yes
```

puis > sudo saxon-xslt -s:SP1326.bash.html -xsl:/home/nicolas/gitlab/templates-sp/XHTML52erudit.xsl -o:SP1326.xml



## TODO

@marcello, @nicolas

1. finaliser templatehtml5 SP
2. finaliser templatehtml5 Stylo generique
3. finaliser templatehtml5 Stylo annotate
4. tous les templates docx, odt, tei
5. Latex : sed pour déplacer les Mots-clés
6. finaliser le bash exportArticle Générique
7. faire le bash exportArticle SP
8. merge toutes les branches book > master


@arthur

0. nouvelle version avec yaml différent, version taggée "Nouveau yaml (mise-à-jour du système Stylo)"
1. update submodule
2. 
