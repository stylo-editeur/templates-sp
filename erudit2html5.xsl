<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"    xmlns:xlink="http://www.w3.org/1999/xlink"    version="2.0"    xpath-default-namespace="http://www.erudit.org/xsd/article"       xmlns="http://www.w3.org/1999/xhtml">
  <xsl:output        method="xml"        doctype-system="about:legacy-compat"        omit-xml-declaration = "yes"        encoding="UTF-8"        indent="yes" />
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:dc="http://purl.org/dc/terms/"            xmlns:foaf="http://xmlns.com/foaf/0.1/" xml:lang="fr">
      <head>
        <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/"/>
        <link rel="schema.MARCREL" href="http://www.loc.gov/loc.terms/relators/"/>
        <link rel="DCTERMS.subject" href="http://rameau.bnf.fr"/>
        <link rel="schema.prism" href="http://prismstandard.org/namespaces/basic/2.0/"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <xsl:comment>Métadonnées Dublin Core </xsl:comment>
        <meta name="DC.type" content="Text"/>
        <meta name="DC.format" content="text/html"/>
        <meta name="DC.type" content="journalArticle"/>
        <meta name="DC.type" class="typeArticle">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/liminaire/grtitre/surtitre2)"                        />
          </xsl:attribute>
        </meta>
        <meta name="DC.title">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/liminaire/grtitre/titre)"/>
          </xsl:attribute>
        </meta>
        <meta name="DC.title.Main">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/liminaire/grtitre/titre)"/>
          </xsl:attribute>
        </meta>
        <xsl:if test="article/liminaire/grtitre/sstitre">
          <meta name="DC.title.Subtitle">
            <xsl:attribute name="content">
              <xsl:value-of select="normalize-space(article/liminaire/grtitre/sstitre)"/>
            </xsl:attribute>
          </meta>
        </xsl:if>
        <meta name="DC.publisher" class="editeur">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/admin/editeur/nomorg)"/>
          </xsl:attribute>
        </meta>
        <meta name="DC.publisher" class="producteur">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/admin/prod/nomorg)"/>
          </xsl:attribute>
        </meta>
        <meta name="DC.publisher" class="producteurNum">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/admin/prodnum/nomorg)"/>
          </xsl:attribute>
        </meta>
        <meta name="DC.publisher" class="diffuseur">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/admin/diffnum/nomorg)"/>
          </xsl:attribute>
        </meta>
        <meta name="DC.language">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/@lang)"/>
          </xsl:attribute>
        </meta>
        <meta name="DC.date" class="completeDate">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/admin/numero/pubnum/date)"/>
          </xsl:attribute>
        </meta>
        <meta name="DC.date" class="year">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/admin/numero/pub/annee)"/>
          </xsl:attribute>
        </meta>
        <meta name="DC.rights">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/admin/droitsauteur[1])"/>
          </xsl:attribute>
        </meta>
        <meta name="DC.source">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/admin/revue/titrerev)"/>
          </xsl:attribute>
        </meta>
        <meta name="DC.identifier" class="issn">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/admin/revue/idissnnum)"/>
          </xsl:attribute>
        </meta>
        <xsl:if test="article/admin/numero/grtheme">
          <meta name="DC.relation.isPartOf">
            <xsl:attribute name="content">
              <xsl:value-of select="normalize-space(article/admin/numero/grtheme/theme)"/>
            </xsl:attribute>
          </meta>
        </xsl:if>
        <xsl:comment>Utilisation de MARCREL pour spécifier le rôle du "contributeur" en Dublin core    drt = director </xsl:comment>
        <meta name="director">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(concat(article/admin/revue/directeur/nompers/prenom, ' ', article/admin/revue/directeur/nompers/nomfamille))"                        />
          </xsl:attribute>
        </meta>
        <meta name="DC.contributor.drt">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(concat(article/admin/revue/directeur/nompers/prenom, ' ', article/admin/revue/directeur/nompers/nomfamille))"                        />
          </xsl:attribute>
        </meta>
        <meta name="MARCREL.drt">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(concat(article/admin/revue/directeur/nompers/prenom, ' ', article/admin/revue/directeur/nompers/nomfamille))"                        />
          </xsl:attribute>
        </meta>
        <xsl:comment>Auteur(s) de l'article </xsl:comment>
        <xsl:if test="article/liminaire/grauteur">
          <xsl:for-each select="article/liminaire/grauteur/auteur">
            <meta name="author">
              <xsl:attribute name="content">
                <xsl:if test="./nomorg">
                  <xsl:value-of select="normalize-space(./nomorg)"/>
                </xsl:if>
                <xsl:if test="./nompers">
                  <xsl:value-of select="normalize-space(concat(./nompers/prenom, ' ', ./nompers/nomfamille))" />
                </xsl:if>
              </xsl:attribute>
            </meta>
            <meta name="DC.creator">
              <xsl:attribute name="content">
                <xsl:if test="./nomorg">
                  <xsl:value-of select="normalize-space(./nomorg)"/>
                </xsl:if>
                <xsl:if test="./nompers">
                  <xsl:value-of select="normalize-space(concat(./nompers/prenom, ' ', ./nompers/nomfamille))" />
                </xsl:if>
              </xsl:attribute>
            </meta>
          </xsl:for-each>
        </xsl:if>
        <xsl:comment>Rédacteur(s)-en-chef du dossier </xsl:comment>
        <xsl:if test="article/admin/numero/grtheme">
          <xsl:for-each select="article/admin/revue/redacteurchef">
            <meta name="DC.contributor.edt">
              <xsl:attribute name="content">
                <xsl:value-of select="normalize-space(concat(./nompers/prenom, ' ', ./nompers/nomfamille))" />
              </xsl:attribute>
            </meta>
            <meta name="MARCREL.edt">
              <xsl:attribute name="content">
                <xsl:value-of select="normalize-space(concat(./nompers/prenom, ' ', ./nompers/nomfamille))" />
              </xsl:attribute>
            </meta>
          </xsl:for-each>
        </xsl:if>
        <xsl:comment>Résumés </xsl:comment>
        <xsl:if test="article/liminaire/resume">
          <xsl:for-each select="article/liminaire/resume">
            <meta name="description">
              <xsl:attribute name="xml:lang">
                <xsl:value-of select="./@lang"/>
              </xsl:attribute>
              <xsl:attribute name="lang">
                <xsl:value-of select="./@lang"/>
              </xsl:attribute>
              <xsl:attribute name="content">
                <xsl:for-each select="alinea">
                  <xsl:value-of select="normalize-space(.)"/><xsl:value-of select="' '"/>
                </xsl:for-each>
              </xsl:attribute>
            </meta>
          </xsl:for-each>
        </xsl:if>
        <xsl:comment>Mots clés auteurs </xsl:comment>
        <xsl:if test="article/liminaire/grmotcle">
          <xsl:for-each select="article/liminaire/grmotcle">
            <meta name="keywords">
              <xsl:attribute name="xml:lang">
                <xsl:value-of select="./@lang"/>
              </xsl:attribute>
              <xsl:attribute name="lang">
                <xsl:value-of select="./@lang"/>
              </xsl:attribute>
              <xsl:attribute name="content">
                <xsl:value-of select="normalize-space(string-join(./motcle, ', '))" />
              </xsl:attribute>
            </meta>
          </xsl:for-each>
        </xsl:if>
        <xsl:comment>Mots clés éditeurs </xsl:comment>
        <xsl:if test="article/admin/infoarticle/grdescripteur">
          <xsl:for-each select="article/admin/infoarticle/grdescripteur/descripteur">
            <meta name="DC.subject" xml:lang="fr" lang="fr">
              <xsl:attribute name="content">
                <xsl:value-of select="node()"/>
              </xsl:attribute>
            </meta>
          </xsl:for-each>
        </xsl:if>
        <xsl:comment>PRISM schema - Publishing Requirements for Industry Standard Metadata - Schema specialise pour les revues et journaux</xsl:comment>
        <meta name="prism.publicationName">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/admin/revue/titrerev)"/>
          </xsl:attribute>
        </meta>
        <meta name="prism.corporateEntity">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/admin/editeur/nomorg)"/>
          </xsl:attribute>
        </meta>
        <xsl:if test="article/admin/numero/grtheme">
          <meta name="prism.issueName">
            <xsl:attribute name="content">
              <xsl:value-of select="normalize-space(article/admin/numero/grtheme/theme)"/>
            </xsl:attribute>
          </meta>
        </xsl:if>
        <meta name="prism.publicationDate">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/admin/numero/pubnum/date)"/>
          </xsl:attribute>
        </meta>
        <meta name="prism.genre">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/liminaire/grtitre/surtitre2)"                        />
          </xsl:attribute>
        </meta>
        <meta name="prism.issn">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/admin/revue/idissnnum)"/>
          </xsl:attribute>
        </meta>
        <xsl:comment>RDFa pour ajout des métadonnées DC</xsl:comment>
        <meta property="dc:format" content="text/html"/>
        <meta property="dc:identifier">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/admin/revue/idissnnum)"/>
          </xsl:attribute>
        </meta>
        <meta property="dc:title">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/liminaire/grtitre/titre)"/>
          </xsl:attribute>
        </meta>
        <meta property="dc:title.Main">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/liminaire/grtitre/titre)"/>
          </xsl:attribute>
        </meta>
        <xsl:if test="article/liminaire/grtitre/sstitre">
          <meta property="dc:title.Subtitle">
            <xsl:attribute name="content">
              <xsl:value-of select="normalize-space(article/liminaire/grtitre/sstitre)"/>
            </xsl:attribute>
          </meta>
        </xsl:if>
        <meta property="dc:publisher">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/admin/editeur/nomorg)"/>
          </xsl:attribute>
        </meta>
        <meta property="dc:language">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/@lang)"/>
          </xsl:attribute>
        </meta>
        <meta property="dc:type" content="journalArticle"/>
        <meta property="dcterm:created">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/admin/numero/pubnum/date)"/>
          </xsl:attribute>
        </meta>
        <meta property="dc:rights">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/admin/droitsauteur[1])"/>
          </xsl:attribute>
        </meta>
        <xsl:if test="article/admin/numero/grtheme">
          <meta property="dc:relation.isPartOf">
            <xsl:attribute name="content">
              <xsl:value-of select="normalize-space(article/admin/numero/grtheme/theme)"/>
            </xsl:attribute>
          </meta>
        </xsl:if>
        <meta property="dc:source">
          <xsl:attribute name="content">
            <xsl:value-of select="normalize-space(article/admin/revue/titrerev)"/>
          </xsl:attribute>
        </meta>
        <xsl:if test="article/liminaire/grauteur">
          <xsl:for-each select="article/liminaire/grauteur/auteur">
            <meta property="dc:creator">
              <xsl:attribute name="content">
                <xsl:if test="./nomorg">
                  <xsl:value-of select="normalize-space(./nomorg)"/>
                </xsl:if>
                <xsl:if test="./nompers">
                  <xsl:value-of select="normalize-space(concat(./nompers/prenom, ' ', ./nompers/nomfamille))" />
                </xsl:if>
              </xsl:attribute>
            </meta>
          </xsl:for-each>
        </xsl:if>
        <xsl:if test="article/liminaire/resume">
          <xsl:for-each select="article/liminaire/resume">
            <meta property="dcterms:abstract">
              <xsl:attribute name="xml:lang">
                <xsl:value-of select="./@lang"/>
              </xsl:attribute>
              <xsl:attribute name="lang">
                <xsl:value-of select="./@lang"/>
              </xsl:attribute>
              <xsl:attribute name="content">
                <xsl:for-each select="alinea">
                  <xsl:value-of select="normalize-space(.)"/><xsl:value-of select="' '"/>
                </xsl:for-each>
              </xsl:attribute>
            </meta>
          </xsl:for-each>
        </xsl:if>
        <xsl:if test="article/admin/infoarticle/grdescripteur">
          <xsl:for-each select="article/admin/infoarticle/grdescripteur/descripteur">
            <meta property="dc:subject" xml:lang="fr" lang="fr">
              <xsl:attribute name="content">
                <xsl:value-of select="node()"/>
              </xsl:attribute>
            </meta>
          </xsl:for-each>
        </xsl:if>
        <xsl:comment>Fin des métadonnées dans le Head</xsl:comment>
        <xsl:comment>Balise Title pour affichage titre dans le navigateur</xsl:comment>
        <title>
          <xsl:value-of select="normalize-space(article/liminaire/grtitre/titre)"/>
        </title>
        <xsl:comment>style et lien vers CSS</xsl:comment>
        <style type="text/css"> code { white-space: pre; }</style>
      </head>
      <xsl:comment> Corps du document </xsl:comment>
      <body>
        <nav id="TOC">
          <ul>
            <xsl:for-each select="//corps/section1">
              <xsl:if test="./titre">
                <li>
                  <a>
                    <xsl:attribute name="href">
                      <xsl:text>#</xsl:text>
                        <xsl:value-of select="(translate(translate(normalize-space(./titre),' ',''), 'ABCDEFGHIJKLMNOPQRSTUVWXYZéèêàâûüîïôö:.’', 'abcdefghijklmnopqrstuvwxyzeeeaauuiioo_-'))"/>
                    </xsl:attribute>
                    <xsl:value-of select="./titre"/>
                  </a>
                  <xsl:if test="./section2">
                    <ul>
                      <xsl:for-each select="./section2">
                        <li>
                          <a>
                            <xsl:attribute name="href">
                              <xsl:text>#</xsl:text>
                                <xsl:value-of select="(translate(translate(normalize-space(./titre),' ',''), 'ABCDEFGHIJKLMNOPQRSTUVWXYZéèêàâûüîïôö:.’', 'abcdefghijklmnopqrstuvwxyzeeeaauuiioo_-'))"/>
                            </xsl:attribute>
                            <xsl:value-of select="./titre"/>
                          </a>
                        </li>
                      </xsl:for-each>
                    </ul>
                  </xsl:if>
                </li>
              </xsl:if>
            </xsl:for-each>
          </ul>
        </nav>
        <xsl:comment>indexations FOAF</xsl:comment>
        <div class="indexations-foaf">
          <xsl:comment>indexation FOAF pour auteur de l'article</xsl:comment>
          <xsl:if test="article/liminaire/grauteur">
            <xsl:for-each select="article/liminaire/grauteur/auteur">
              <div property="http://purl.org/dc/terms/creator">
                  <xsl:if test="./nomorg">
                    <div vocab="http://xmlns.com/foaf/0.1/" typeof="Organization" class="foaf-author">
                      <span property="organizationName">
                        <xsl:value-of select="normalize-space(./nomorg)"/>
                      </span>
                    </div>
                  </xsl:if>
                  <xsl:if test="./nompers">
                    <div vocab="http://xmlns.com/foaf/0.1/" typeof="Person" class="foaf-author">
                      <span property="familyName">
                        <xsl:value-of select="./nompers/nomfamille"/>
                      </span>
                      <span property="firstName">
                        <xsl:value-of select="./nompers/prenom"/>
                      </span>
                    </div>
                  </xsl:if>
              </div>
            </xsl:for-each>
          </xsl:if>
          <xsl:comment>indexation FOAF pour redacteur du dossier</xsl:comment>
          <xsl:if test="article/admin/numero/grtheme">
            <xsl:for-each select="article/admin/revue/redacteurchef">
              <div property="http://purl.org/dc/terms/contributor.edt">
                <div vocab="http://xmlns.com/foaf/0.1/" typeof="Person"     class="foaf-redactor">
                  <span property="familyName">
                    <xsl:value-of select="./nompers/nomfamille"/>
                  </span>
                  <span property="firstName">
                    <xsl:value-of select="./nompers/prenom"/>
                  </span>
                  <xsl:if test="./@sexe">
                    <span property="gender">
                      <xsl:value-of select="./@sexe"/>
                    </span>
                  </xsl:if>
                </div>
              </div>
            </xsl:for-each>
          </xsl:if>
          <xsl:comment>indexation FOAF pour directeur de la revue</xsl:comment>
          <div property="http://purl.org/dc/terms/contributor.drt">
            <div vocab="http://xmlns.com/foaf/0.1/" typeof="Person" class="foaf-director">
              <span property="familyName">
                <xsl:value-of select="article/admin/revue/directeur/nompers/nomfamille"/>
              </span>
              <span property="firstName">
                <xsl:value-of select="article/admin/revue/directeur/nompers/prenom" />
              </span>
              <span property="gender">
                <xsl:value-of select="article/admin/revue/directeur/@sexe"/>
              </span>
            </div>
          </div>
        </div>
        <xsl:comment>Integration schema "ScholarlyArticle"</xsl:comment>
        <div vocab="http://schema.org/" typeof="ScholarlyArticle" resource="#article" id="schema-scholarly-article">
          <span property="name">
            <xsl:value-of select="/article/liminaire/grtitre/titre"/>
            <br/>
          </span>
          <xsl:comment>Indexation auteur de l'article</xsl:comment>
          <span property="author">
            <xsl:for-each select="article/liminaire/grauteur/auteur">
              <xsl:if test="./nomorg">
                <span property="organizationName">
                    <xsl:value-of select="normalize-space(./nomorg)"/>
                </span>
              </xsl:if>
              <xsl:if test="./nompers">
                <span property="familyName">
                  <xsl:value-of select="./nompers/nomfamille"/>
                </span>
                <span property="firstName">
                  <xsl:value-of select="./nompers/prenom"/>
                </span>
              </xsl:if>
              <br/>
            </xsl:for-each>
          </span>
          <xsl:comment>Indexation editeur</xsl:comment>
          <span resource="#periodical">
            <span property="publisher">
              <xsl:value-of select="/article/admin/editeur/nomorg"/>
            </span>
            <br />
            <span property="issn">
              <xsl:value-of select="/article/admin/revue/idissnnum"/>
            </span>
          </span>
          <xsl:comment>Indexation revue</xsl:comment>
          <div property="isPartOf" typeof="PublicationIssue" resource="#issue">
            <span typeof="Periodical" resource="#periodical">
              <span property="name">
                <xsl:value-of select="/article/admin/prod/nomorg"/>
              </span>
              <span property="datePublished">
                <xsl:value-of select="/article/admin/numero/pubnum/date"/>
              </span>
            </span>
          </div>
          <xsl:comment>Indexation dossier</xsl:comment>
          <span property="isPartOf" typeof="PublicationVolume" resource="#periodical" class="titreDossier">
            <xsl:value-of select="/article/admin/numero/grtheme/theme"/>
          </span>
          <xsl:comment>Indexation resumes</xsl:comment>
          <xsl:if test="article/liminaire/resume">
            <xsl:for-each select="article/liminaire/resume">
              <div class="description">
                <div property="description" class="resume">
                  <xsl:attribute name="lang">
                    <xsl:value-of select="./@lang"/>
                  </xsl:attribute>
                    <p>
                      <xsl:apply-templates select="alinea[1]"/>
                      <xsl:for-each select="./alinea[position()>1]">
                        <br/>
                        <xsl:apply-templates/>
                      </xsl:for-each>
                    </p>
                </div>
              </div>
            </xsl:for-each>
          </xsl:if>
          <xsl:comment>Indexation mots cles controles</xsl:comment>
          <xsl:if test="article/admin/infoarticle/grdescripteur/descripteur">
            <div class="keywords" vocab="https://purl.org/dc/terms/">
              <xsl:for-each select="//grdescripteur/descripteur">
                <div>
                  <xsl:attribute name="class">
                    <xsl:value-of select="."/>
                  </xsl:attribute>
                  <xsl:attribute name="lang">fr</xsl:attribute>
                  <span property="subject" class="label">
                    <xsl:value-of select="."/>
                  </span>
                  <span property="subject" class="uriRameau">
                  </span>
                  <span property="subject" class="idRameau">
                  </span>
                </div>
              </xsl:for-each>
            </div>
          </xsl:if>
        </div>
        <xsl:comment>end Integration schema "ScholarlyArticle"</xsl:comment>
        <xsl:comment>Indexation mots cles auteurs</xsl:comment>
        <xsl:if test="article/liminaire/grmotcle">
          <xsl:for-each select="article/liminaire/grmotcle">
            <div>
              <xsl:attribute name="class">authorKeywords_<xsl:value-of select="./@lang"/>
            </xsl:attribute>
            <xsl:attribute name="vocab">http://purl.org/dc/terms/</xsl:attribute>
            <xsl:attribute name="lang">
              <xsl:value-of select="./@lang"/>
            </xsl:attribute>
            <span property="subject">
              <xsl:value-of select="normalize-space(string-join(./motcle, ', '))" />
            </span>
          </div>
        </xsl:for-each>
      </xsl:if>
      <xsl:comment> article et parties annexes </xsl:comment>
      <article>
      <xsl:apply-templates select=".//corps"/>
      <xsl:if test=".//partiesann/grbiblio">
        <xsl:apply-templates select=".//partiesann/grbiblio/node()"/>
      </xsl:if>
      <xsl:if test=".//partiesann/grnote">
        <section class="footnotes">
          <hr/>
          <ol>
            <xsl:for-each select=".//partiesann/grnote/note">
              <li>
                <xsl:attribute name="id">
                  <xsl:value-of select="concat('fn', ./no)"/>
                </xsl:attribute>
                <p>
                  <xsl:apply-templates select="./alinea/node()"/>
                  <a class="footnote-back">
                    <xsl:attribute name="href">
                      <xsl:value-of select="concat('#fnref', ./no)"/>
                    </xsl:attribute>
                    ↩
                  </a>
                </p>
              </li>
            </xsl:for-each>
          </ol>
        </section>
      </xsl:if>
    </article>
    </body>
  </html>
</xsl:template>

<xsl:template match="corps">
  <xsl:comment> Corps de l'article </xsl:comment>

    <xsl:if test="//liminaire/notegen[@typenoteg='edito']">
      <xsl:apply-templates select="//liminaire/notegen[@typenoteg='edito']"/>
    </xsl:if>
    <xsl:if test="//liminaire//dedicace">
      <xsl:apply-templates select="//liminaire//dedicace"/>
    </xsl:if>
    <xsl:apply-templates />

</xsl:template>

<xsl:template match="notegen[@typenoteg='edito']">
  <p>
    <span class="notice">
        <xsl:apply-templates select="alinea[1]"/>
        <xsl:for-each select="./alinea[position()>1]">
          <br/>
          <xsl:apply-templates/>
        </xsl:for-each>
    </span>
  </p>
</xsl:template>

<xsl:template match="dedicace">
  <p>
    <span class="dedicace">
        <xsl:apply-templates select="alinea[1]"/>
        <xsl:for-each select="./alinea[position()>1]">
          <br/>
          <xsl:apply-templates/>
        </xsl:for-each>
    </span>
  </p>
</xsl:template>

<xsl:template match="//corps//epigraphe">
  <p>
    <span class="epigraphe">
        <xsl:apply-templates select="alinea[1]"/>
        <xsl:for-each select="./alinea[position()>1]">
          <br/>
          <xsl:apply-templates/>
        </xsl:for-each>
    </span>
  </p>
  <xsl:apply-templates select="source"/>
</xsl:template>

<xsl:template match="//source[not(parent::figure) and not(parent::epigraphe)]">
  <span class="citation">
    <xsl:value-of select="."/>
  </span>
</xsl:template>
<xsl:template match="//section1">
  <section>
    <xsl:if test="./titre">
      <xsl:attribute name="id">
        <xsl:value-of select="(translate(translate(normalize-space(./titre),' ',''), 'ABCDEFGHIJKLMNOPQRSTUVWXYZéèêàâûüîïôö:.’', 'abcdefghijklmnopqrstuvwxyzeeeaauuiioo_-'))"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:attribute name="class">level2</xsl:attribute>
    <xsl:apply-templates/>
  </section>
</xsl:template>
<xsl:template match="//section2">
  <section>
    <xsl:if test="./titre">
      <xsl:attribute name="id">
        <xsl:value-of select="(translate(translate(normalize-space(./titre),' ',''), 'ABCDEFGHIJKLMNOPQRSTUVWXYZéèêàâûüîïôö:.’', 'abcdefghijklmnopqrstuvwxyzeeeaauuiioo_-'))"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:attribute name="class">level3</xsl:attribute>
    <xsl:apply-templates/>
  </section>
</xsl:template>
<xsl:template match="//section3">
  <section>
    <xsl:if test="./titre">
      <xsl:attribute name="id">
        <xsl:value-of select="(translate(translate(normalize-space(./titre),' ',''), 'ABCDEFGHIJKLMNOPQRSTUVWXYZéèêàâûüîïôö:.’', 'abcdefghijklmnopqrstuvwxyzeeeaauuiioo_-'))"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:attribute name="class">level4</xsl:attribute>
    <xsl:apply-templates/>
  </section>
</xsl:template>
<xsl:template match="//section4">
  <section>
    <xsl:if test="./titre">
      <xsl:attribute name="id">
        <xsl:value-of select="(translate(translate(normalize-space(./titre),' ',''), 'ABCDEFGHIJKLMNOPQRSTUVWXYZéèêàâûüîïôö:.’', 'abcdefghijklmnopqrstuvwxyzeeeaauuiioo_-'))"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:attribute name="class">level5</xsl:attribute>
    <xsl:apply-templates/>
  </section>
</xsl:template>
<xsl:template match="//section1/titre">
  <h2>
    <xsl:apply-templates/>
  </h2>
</xsl:template>
<xsl:template match="//section2/titre">
  <h3>
    <xsl:apply-templates/>
  </h3>
</xsl:template>
<xsl:template match="//section3/titre">
  <h4>
    <xsl:apply-templates/>
  </h4>
</xsl:template>
<xsl:template match="//section4/titre">
  <h5>
    <xsl:apply-templates/>
  </h5>
</xsl:template>
<xsl:template match="para">
  <p>
    <xsl:apply-templates/>
  </p>
</xsl:template>
<xsl:template match="para/alinea">
  <xsl:apply-templates/>
</xsl:template>
<xsl:template match="alinea[parent::biblio or parent::divbiblio]">
  <p><xsl:apply-templates/></p>
</xsl:template>
<xsl:template match="dedicace/alinea[1]">
  <xsl:apply-templates/>
</xsl:template>
<xsl:template match="notegen/alinea[1]">
  <xsl:apply-templates/>
</xsl:template>
<xsl:template match="epigraphe/alinea[1]">
  <xsl:apply-templates/>
</xsl:template>
<xsl:template match="resume/alinea[1]">
  <xsl:apply-templates/>
</xsl:template>
<xsl:template match="epigraphe/source">
  <p><span class="source"><xsl:apply-templates/></span></p>
</xsl:template>
<xsl:template match="marquage[normalize-space(@typemarq) = 'italique']">
  <em>
    <xsl:apply-templates/>
  </em>
</xsl:template>
<xsl:template match="marquage[normalize-space(@typemarq) = 'gras']">
  <strong>
    <xsl:apply-templates/>
  </strong>
</xsl:template>
<xsl:template match="exposant">
  <sup>
    <xsl:apply-templates/>
  </sup>
</xsl:template>
<xsl:template match="indice">
  <sub>
    <xsl:apply-templates/>
  </sub>
</xsl:template>
<xsl:template match="bloccitation">
  <blockquote>
    <xsl:apply-templates/>
  </blockquote>
</xsl:template>
<xsl:template match="bloccitation//alinea">
  <p>
    <xsl:apply-templates/>
  </p>
</xsl:template>
<xsl:template match="elemliste//alinea">
  <xsl:apply-templates/>
</xsl:template>
<xsl:template match="citation">
  <q>
    <xsl:apply-templates/>
  </q>
</xsl:template>
<xsl:template match="listeord">
  <ol>
    <xsl:apply-templates/>
  </ol>
</xsl:template>
<xsl:template match="listenonord">
  <ul>
    <xsl:apply-templates/>
  </ul>
</xsl:template>
<xsl:template match="elemliste">
  <li>
    <xsl:apply-templates/>
  </li>
</xsl:template>
<xsl:template match="source">
  <p class="source">
    <xsl:apply-templates/>
  </p>
</xsl:template>
<xsl:template match="renvoi">
  <a class="footnote-ref">
    <xsl:attribute name="href">
      <xsl:value-of select="normalize-space(concat('#fn', substring-before(substring-after(@idref, 'sdfootnote'),'sym')))"/>
    </xsl:attribute>
    <xsl:attribute name="id">
      <xsl:value-of select="normalize-space(concat('fnref', substring-before(substring-after(@idref, 'sdfootnote'),'sym')))"/>
    </xsl:attribute>
    <sup>
      <xsl:apply-templates/>
    </sup>
  </a>
</xsl:template>
<xsl:template match="liensimple">
  <a class="uri" href="{@xlink:href}">
    <xsl:apply-templates/>
  </a>
</xsl:template>
<xsl:template match="//figure">
  <figure>
    <xsl:if test="/objetmedia">
      <xsl:apply-templates/>
    </xsl:if>
    <xsl:if test="descendant::image">
      <xsl:for-each select="descendant::image">
        <img>
          <xsl:attribute name="src">
            <xsl:value-of select="normalize-space(concat('.//media/', ./@id))" />
          </xsl:attribute>
        </img>
      </xsl:for-each>
    </xsl:if>
    <xsl:if test="(descendant::legende) or (descendant::no)">
      <figcaption>
        <xsl:apply-templates select="descendant::no/node()"/>
        <xsl:if test="(descendant::legende) and (descendant::no)">
          <xsl:value-of>. </xsl:value-of>
        </xsl:if>
        <xsl:apply-templates select="descendant::legende/alinea/node()"/>
      </figcaption>
    </xsl:if>
    <xsl:if test="descendant::source">
      <p><span class="source">
        <xsl:apply-templates select="descendant::source/node()"/>
        </span>
      </p>
    </xsl:if>
  </figure>
</xsl:template>

  <xsl:template match="tableau">

    <xsl:if test="child::objetmedia">
      <figure class="tableau">
      <xsl:if test="descendant::image">
        <xsl:for-each select="descendant::image">
          <img>
            <xsl:attribute name="src">
              <xsl:value-of select="normalize-space(concat('.//media/', ./@id))" />
            </xsl:attribute>
          </img>
        </xsl:for-each>
      </xsl:if>
      <xsl:if test="(descendant::legende) or (descendant::no)">
        <figcaption>
          <xsl:apply-templates select="descendant::no/node()"/>
          <xsl:if test="(descendant::legende) and (descendant::no)">
            <xsl:value-of>. </xsl:value-of>
          </xsl:if>
          <xsl:apply-templates select="descendant::legende/alinea/node()"/>
        </figcaption>
      </xsl:if>
      </figure>
    </xsl:if>

    <xsl:if test="child::tabtexte">
      <table>
        <xsl:apply-templates/>
      </table>
    </xsl:if>

  </xsl:template>

  <xsl:template match="tableau/no">
  </xsl:template>

  <xsl:template match="legende">
    <caption>
      <xsl:value-of select="./../no"/>
      <xsl:text> : </xsl:text>
      <xsl:choose>
       <xsl:when test="./alinea/marquage">
         <xsl:value-of select="./alinea/marquage"/>
       </xsl:when>
       <xsl:otherwise>
         <xsl:value-of select="./alinea"/>
       </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="./alinea/renvoi"/>
    </caption>
  </xsl:template>

  <xsl:template match="tabtexte">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="tabentete">
    <xsl:for-each select="./tabligne">
      <tr>
        <xsl:apply-templates/>
      </tr>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="tabgrligne">
    <xsl:for-each select="./tabligne">
      <tr>
        <xsl:apply-templates/>
      </tr>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="tabcelluled">
    <xsl:choose>
      <xsl:when test="ancestor::tabentete">
        <th><xsl:value-of select=".//marquage"/></th>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test=".//marquage">
            <td><xsl:apply-templates select=".//marquage"/></td>
          </xsl:when>
          <xsl:otherwise>
            <td><xsl:value-of select=".//alinea"/></td>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


  <!--<xsl:template match=""/>-->

<xsl:template match="//biblio">
  <section class="level2 unnumbered">
    <xsl:attribute name="id">
      <xsl:if test="child::titre">
        <xsl:value-of select="(translate(translate(normalize-space(./titre),' ',''), 'ABCDEFGHIJKLMNOPQRSTUVWXYZéèêàâûüîïôö:.’', 'abcdefghijklmnopqrstuvwxyzeeeaauuiioo_-'))"/>
      </xsl:if>
      <xsl:if test="not(child::titre)">
        <xsl:value-of>bibliographie</xsl:value-of>
      </xsl:if>
    </xsl:attribute>
    <xsl:apply-templates select="titre"/>
    <div id="refs" class="references">
      <xsl:apply-templates select="child::*[not(self::titre)]"/>
    </div>
  </section>
</xsl:template>
<xsl:template match="//biblio/titre">
  <h2>
    <xsl:apply-templates/>
  </h2>
</xsl:template>
<xsl:template match="//biblio//divbiblio">
  <section class="level3 unnumbered">
    <xsl:attribute name="id">
      <xsl:if test="child::titre">
        <xsl:value-of select="(translate(translate(normalize-space(./titre[text()]),' ',''), 'ABCDEFGHIJKLMNOPQRSTUVWXYZéèêàâûüîïôö:.’', 'abcdefghijklmnopqrstuvwxyzeeeaauuiioo_-'))"/>
      </xsl:if>
      <xsl:if test="not(child::titre)">
        <xsl:value-of>bibliographie</xsl:value-of>
      </xsl:if>
    </xsl:attribute>
    <xsl:apply-templates/>
  </section>
</xsl:template>
<xsl:template match="//biblio//divbiblio/titre">
  <h3>
    <xsl:apply-templates/>
  </h3>
</xsl:template>
<xsl:template match="//biblio//refbiblio">
  <p>
    <xsl:apply-templates/>
  </p>
</xsl:template>
<xsl:template match="*">
  <xsl:copy-of select="."/>
</xsl:template>
</xsl:stylesheet>
