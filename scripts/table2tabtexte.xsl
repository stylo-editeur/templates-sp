<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY NonBreakingSpace "&#160;"> ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:h="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="h" version="2.0" xpath-default-namespace="http://www.w3.org/1999/xhtml"
  xmlns="http://www.erudit.org/xsd/article">

<!-- By default, copy elements and attributes unchanged -->
<xsl:template match="node()|@*">
  <xsl:copy>
    <xsl:apply-templates select="node()|@*"/>
  </xsl:copy>
</xsl:template>


<!-- ce script a été fourni par Sophy (Erudit) pour transformer les tableaux HTML en tableaux eruditArticle -->
<xsl:template match="table">
  <tableau>
    <legende lang="fr"><titre>Tableau</titre></legende>
    <tabtexte lang="fr" type="5">
      <xsl:apply-templates/>
    </tabtexte>
  </tableau>
</xsl:template>
<xsl:template match="colgroup">
  <tabgrcol>
    <xsl:apply-templates/>
  </tabgrcol>
</xsl:template>
<xsl:template match="col">
  <tabcol>
    <xsl:apply-templates/>
  </tabcol>
</xsl:template>
<xsl:template match="thead">
  <tabentete>
    <xsl:apply-templates/>
  </tabentete>
</xsl:template>
<xsl:template match="tr">
  <tabligne>
    <xsl:apply-templates/>
  </tabligne>
</xsl:template>
<xsl:template match="th">
  <tabcellulee>
    <alinea>
      <xsl:apply-templates/>
    </alinea>
  </tabcellulee>
</xsl:template>
<xsl:template match="tbody">
  <tabgrligne>
    <xsl:apply-templates/>
  </tabgrligne>
</xsl:template>
<xsl:template match="td">
  <tabcelluled>
    <alinea>
      <xsl:apply-templates/>
    </alinea>
  </tabcelluled>
</xsl:template>
<xsl:template match="em">
  <marquage typemarq="italique">
    <xsl:apply-templates/>
  </marquage>
</xsl:template>
<xsl:template match="strong">
  <marquage typemarq="gras">
    <xsl:apply-templates/>
  </marquage>
</xsl:template>
<xsl:template match="a">
  <xsl:choose>
    <xsl:when test="./@href[starts-with(., '#ref-')]">
      <xsl:apply-templates/>
    </xsl:when>
    <xsl:otherwise>
      <liensimple xlink:type="simple" xlink:href="{@href}">
        <xsl:apply-templates/>
      </liensimple>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>
<xsl:template match="a[normalize-space(@class) = 'footnote-ref']">
  <renvoi typeref="note">
    <xsl:attribute name="idref">
      <xsl:value-of select="concat('sdfootnote', substring-after(@id, 'fnref'), 'sym')"/>
    </xsl:attribute>
    <xsl:apply-templates/>
  </renvoi>
</xsl:template>
<xsl:template match="span[@class='citation']">
  <source>
    <xsl:apply-templates/>
  </source>
</xsl:template>
<xsl:template match="sup">
  <exposant>
    <xsl:apply-templates/>
  </exposant>
</xsl:template>



<!-- <xsl:template match="alinea">
  <p>
    <xsl:apply-templates/>
  </p>
</xsl:template> -->
</xsl:stylesheet>
