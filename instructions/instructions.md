# Transformation XML Erudit > HTML5 Sens Public

## Mission
Il s'agit d'adapter et de finaliser le script XSLT transformant les articles SP au format XML Erudit en article SP au format HTML5.

## Scripts existants

- `erudit2html5.xsl` : **Script à travailler**. À ce stade, c'est une copie du script suivant.
- `Erudit2HTML.xsl` : ce script transforme le XML Erudit en article SP au format HTML4. Il est incomplet, mais il est une bonne base pour démarrer. Il s'agit de l'adapter pour cibler le nouveau "schéma" HTML5 des articles SP.
- `XHTML2eruditV2.xsl` : ce script transforme les articles SP HTML4 en XML Erudit. Il est incomplet, mais fonctionnait à peu près.
- `XHTML52erudit.xsl` : ce script transforme les articles SP HTML5 en XML Erudit. Il est le plus récent et le plus complet.

Pour rappel, la chaine Pandoc a été mise à jour en 2019 :

1. 2016-2018 : utilisation de Pandoc 1.9 avec le template `templateHtmlDcV2.html5`
2. 2019 : utilisation de Pandoc 2.4 avec le template  `templateHtml5.html5`

Il peut être utile de comparer le dernier template HTML5 SP `templateHtml5.html5` avec l'ancien template `templateHtmlDcV2.html5`. Et de comparer les exports HTML produit par chacune des chaines.

## Articles disponibles

On dispose de plusieurs types d'articles selon leur ancienneté de publication :

- des htmls de 2017 produits avec la chaine Pandoc 1.9, et leurs équivalents xml Erudit validés par Erudit (utile pour avoir des xml valides)
  - SP1240.html (2017 - Pandoc 1.9)
  - SP1267.html (2017 - Pandoc 1.9)
  - SP1282.html (2017 - Pandoc 1.9)
  - SP1287.html (2017 - Pandoc 1.9)
  - 1240-article.xml (validé par Erudit)
  - 1267-article.xml (validé par Erudit)
  - 1282-article.xml (validé par Erudit)
  - 1287-article.xml (validé par Erudit)
- des htmls de 2018 produits avec la chaine Pandoc 1.9 et leur équivalents html produits avec la chaine 2.4 (utile pour comparer les diff. entre les deux chaines)
  - SP1290.html (2018 - Pandoc 1.9)
  - SP1291.html (2018 - Pandoc 1.9)  
  - SP1326.html (2018 - Pandoc 1.9)
  - SP1290.bash.html (2018 - Pandoc 2.4)
  - SP1291.bash.html (2018 - Pandoc 2.4)
  - SP1326.bash.html (2018 - Pandoc 2.4)
- des xmls de 2003 à 2015, validés par Erudit, qu'il faudra ensuite transformer. À fournir quand le script sera "prêt".

## Comment travailler :

1. cloner le répertoire templates-sp
2. changer de branche: `git checkout xml2Html`
3. ne faire de modifications que sur le fichier `XMLErudit2Html5.xsl` (si identification d'une erreur ou amélioration sur un autre script, le suggérer en issue)
4. documenter au mieux les commits
5. pusher sur la branch `xml2Html`

Outil conseillé pour comparer les templates ou les fichiers html/xml : http://meldmerge.org/

Suggestions :
- commencer par avoir un corps de texte correctement structuré : section/h2, h3, etc.
- placer les footnotes et bibliographie au bon endroit
- affiner la gestion des métadonnées, que ce soit dans le `<head>` ou dans les métadonnées foaf/scholarlyArticle dans le `<body>`
- affiner le corps du texte et tous les cas possibles en terme de métadonnées.

## Transformations

### Transformation HTML5 2 Erudit :

exemple de commande sur ma machine (adapter les chemins en fonction)

      java -cp /usr/share/maven-repo/net/sf/saxon/Saxon-HE/9.8.0.8/Saxon-HE-9.8.0.8.jar:/usr/bin/tagsoup-1.2.1.jar net.sf.saxon.Transform -x:org.ccil.cowan.tagsoup.Parser  -s:SP1326.bash.html -xsl:/home/nicolas/gitlab/templates-sp/XHTML52erudit.xsl -o:SP1326.xml \!indent=yes

Il faut donc utiliser les deux paquets java :
  - Saxon-HE-9.8.0.8.jar
  - tagsoup-1.2.1.jar (utilisé pour parser le html)

### Transformation Erudit 2 HTML5

exemple de commande

      java  -jar /usr/share/maven-repo/net/sf/saxon/Saxon-HE/9.8.0.8/Saxon-HE-9.8.0.8.jar -s:input.xml -xsl:../templates/erudit2Html5.xsl -o:output.html

Un seul paquet cette fois-ci.

## Schéma Erudit

- Une structure simplifiée du schéma Erudit est disponible dans le fichier [structureArticleErudit.md](./instructions/structureArticleErudit.md).
- La documentation du schéma est disponible ici : http://retro.erudit.org/xsd/article/3.0.0/en/doc/schemas/eruditarticle_xsd/schema-summary.html#r1
- Attention: Erudit utilise une version légèrement modifiée pour valider les xml que nous produisons (car ils ne peuvent être complets/finaux que lorsqu'ils sont passés dans le système d'Erudit.) Ce schéma modifié pourrait être nécessaire si le travail est fait avec Oxygen par exemple.

# Transformation des fichiers (28mars 2019)

Le script est mûr pour une transformation grandeur nature.
Les données à transformer sont sur le repo `SP-Archives/erudit/`. Il s'agit des xmls fournit à Erudit, puis corrigés et validés par Erudit. Ce sont eux qui sont actuellement publiés sur https://www.erudit.org/fr/revues/sp/.

2016 et 2017 ont été nativement édités en md, donc il n'est pas nécessaire de les transformer en html, par contre ces années peuvent être utiles justement pour comparer ce que produit ton script avec les html issus de pandoc. Il y aura nécessairement des différences, du fait d'une double normalisation (html>erudit>html). Mais il est important qu'on note quelque part ces différences, et qu'en fonction on affine telle donnée, ou tel ligne du script.

2015 et 2014 on été édités en xml. Notre objectif est d'obtenir des html qui ressemblent le plus possible à nos html 2016/2017.

Je vais te donner un accès à SP-Archives. Je te propose de travailler sur une branche spécifique (`xml2html` pour rester cohérent.).
Dans le dossier `erudit`, les fichiers sont organisés par année, puis on dispose pour chaque année d'un dossier images comprenant toutes les images de l'année.

La structure finale doit être la suivante :

SP-archives/
  - erudit/
    - 2014/
      - images/
      - XXXX-article.xml
      - XXXX-article.pdf
      - ...
    - 2015/
      - images/
      - XXXX-article.xml
      - XXXX-article.pdf
      - ...
  - erudithtml/
    - 2014/
      - SPXXXX/
        - media/
        - SPXXXX.html
      - SPXXXY/
        - media/
        - SPXXXY.html
    - 2015/
      - SPXXXX/
        - media/
        - SPXXXX.html
      - SPXXXY/
        - media/
        - SPXXXY.html

Donc on laisse le dossier `erudit/` tel quel. Et on remplit un dossier `erudithtml/` organisé par année, puis par article (`SPXXXX`).
