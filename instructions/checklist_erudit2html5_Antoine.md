## Ce que j'ai fait

### Commit #1 - 8 mars

- ajouté des éléments <section/> pour respecter la structure du fichier XML et conserver la hiérarchie des niveaux de section XML avec des attributs de classe
 
>  @nicolas: 
>    - attention à bien respecter les classes et autres attributs qui s'ajoutent à chaque balise section.
>    - attention aussi à la balise `<article>`

- corrigé la sortie des commentaires pour le fichier HTML avec <xsl:comment/> 
(les commentaires <!-- --> n'étaient pas traités comme du texte à intégrer dans le fichier de sortie)

>  @nicolas : OK

- ajouté les métadonnées d'indexation dans le body -- tout ce que contient le schema Scholarly Article
	- Integration schema ScholarlyArticle
	- Indexation auteur de l'article
	- Indexation editeur
	- Indexation revue
	- Indexation dossier
	- Indexation resumes
	- Indexation mots cles controles


- Ajouté les mots-clefs auteurs

>  @nicolas : OK super. je vois que tu testes en fonction de la langue +1

- Attribué la valeur de l'élément grtheme dans le fichier XML au nom de dossier dans le fichier HTML, dans 'indexation dossier', c'est ce qui m'a semblé le plus logique, mais je ne sais pas si c'est une méthode solide

 > @nicolas : à vérifier sur les données.

### Commit #2 - 10 mars

- Corriger l'élément <div class="article"/> conformément aux retours que tu as formulés (<article/>)

- Modifié les attributs des sections avec class="levelX" et id="le-titre-de-la-section"

### Commit #3 - 20 mars

 - Créé le scénario de transformation de l'élément <epigraphe/> et modifié la sortie des éléments <source/> pour la citation. 

 - Créé le gabarit pour les <notegen typenoteg="edito"/> qui deviennent <span class="notice"/> 

 - Créé le scénario pour les dedications qui deviennent <span class="dedicace"/>, mais je vais attendre de recevoir les archives pour vérifier que la transformation est appropriée, je n'ai pas pu la tester.

 - Créé la table de matières dans un <nav id="TOC"/> en début d'article.


## Ce que je n'arrive pas à faire:

- Intégrer les identifiants openID, puisque le fichier XML d'entrée ne contient aucune information relative à ces identifiants
- Intégrer les identifiants Rameau, aussi parce que ces identifiants ne figurent pas dans le fichier XML

>  @nicolas : ce sont des données que nous n'avons pas dans les XML. Donc on ne les aura pas dans le html. Pas besoin de les traiter.

> @antoinefrancis : C'est noté.

## Questions 

- Est-ce que je devrais ajouter une table des matières comme dans les fichiers .bash.html du dossier samples?

> @nicolas: Ca serait idéal, dans une balise <nav>, mais je propose que tu fasses cela dans un second temps, concentre toi sur la structure du corps de texte (attention aux diff. de html entre la version ancienne et nouvelle, utilise un outil de comparaison (meld)).

> @antoinefrancis : D'accord.

- Est-ce que la hiérarchie des niveaux de section devrait être représentée dans les fichiers à la <section class=section2></section>?

> @nicolas : je te renvoie aux exemples html5. La hiérarchie est de toutes les façons présente dans les titres h2, h3, etc.

- utiliser <main/> pour le corps de l'article plutôt qu'une <div class="article"/>?

> @nicolas : `<article>` je te renvois aux exemples ou au template pandoc html5 utilisé.

- traduire le genre des auteurs dans la section FOAF (avec un <xsl:when/> par exemple)?

> @nicolas : précise la question stp

> @antoinefrancis : Caduque!

