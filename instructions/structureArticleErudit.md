# Structure d'un article dans le schéma Erudit.

```xml
<article>
  <admin>
    <infoarticle>
      - grdescripteur (groupe de mot-clé "éditeur")
      - nbxxx: nombres d'éléments, pas utile pour le html cible
    </infoarticle>
    <revue>
      - information de la revue
      - pubnum: date de publication
    </revue>
  </admin>
  <liminaire>
    - grtitre: titres
      - surtitre: titre du dossier si l'article est dans un dossier, sinon "Varia".
      - type d'article (Essai, Entretien, Lecture, Création, Sommaire dossier)
      - titre : de l'article
      - sstitre : de l'article
      - trefbiblio : si l'article est une Lecture, on indique ici la référence de l'ouvrage/article analysé.
    - grauteur : groupe d'auteurs
    - resumes
    - grmotcle : groupe de mot-clés auteurs
    - notegen : note d'éditeur (on va la retrouver dans le html cible dans le corps de l'article avec un span "credits" ou "notice")
  </liminaire>
  <corps>
  <section1> en xml, la première section peut ne pas avoir de titre.
    <para></para>
  </section1>
  <section1> les suivantes ont forcément un titre.
    <titre></titre>
    <para></para>
    <section2>
      <titre></titre>
      <para></para>
    </section2>
  </section1>
  </corps>
  <partiesann> pour parties annexes avec
    - bibliographie
    - notes de bas de page
  </partiesann>
</article>
```
