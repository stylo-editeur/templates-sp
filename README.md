# templates-sp

Templates et feuilles de style utilisés dans l'instance _Process_ pour la revue Sens Public.


## Description des templates :

- `templateHtmlDcV2.html5` : conversion pandoc md>html
- `templateLaTeX.latex` : conversion pandoc md>tex
- `XHTML2eruditV2.xsl` : conversion xslt html>xmlErudit
- `Erudit2HTML.xsl` : conversion xmlErudit>html5 (traitement des archives SP)
- `teiV0.template` : conversion pandoc md>xmlTEI
- `chicagomodified.csl` : feuille de style CSL modifiée pour formatter la bibliographie.

